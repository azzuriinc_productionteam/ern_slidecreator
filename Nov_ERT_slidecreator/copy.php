<?php

// --------------------------------------------------

// 背景画像名
$bgName = 'bg';
// 背景画像の拡張子
$bgExtension = '.jpg';
$thumbExtension = '.png';

// --------------------------------------------------
// ディレクトリ階層以下のコピー
// 引数: コピー元ディレクトリ、コピー先ディレクトリ
// 戻り値: 結果
function dir_copy($dir_name, $new_dir) {
  if (!is_dir($new_dir)) {
    mkdir($new_dir);
  }

  if (is_dir($dir_name)) {
    if ($dh = opendir($dir_name)) {
      while (($file = readdir($dh)) !== false) {
        if ($file == '.' || $file == '..') {
          continue;
        }
        if (is_dir($dir_name . '/' . $file)) {
          dir_copy($dir_name . '/' . $file, $new_dir . '/' . $file);
        }
        else {
          copy($dir_name . '/' . $file, $new_dir . '/' . $file);
        }
      }
      closedir($dh);
    }
  }
  return true;
}

// --------------------------------------------------
//  resultディレクトリの初期化

function unlinkRecursive( $dir, $deleteRootToo = false ) {
  if(!$dh = @opendir($dir)) { return; }
  while (false !== ($obj = readdir($dh))) {
    if($obj == '.' || $obj == '..') { continue; }  
    if (!@unlink($dir . '/' . $obj)) { unlinkRecursive($dir.'/'.$obj, true); }
  }
  closedir($dh);
  if ($deleteRootToo) { @rmdir($dir); }
  return;
}

// --------------------------------------------------

$cd = __DIR__;
$pathOfCsv = "contents/csv/namelist.csv";

// CSV: 読み込み
$csv = file_get_contents ( $pathOfCsv );

// CSV: 改行でバラす
$csv = explode ( "\n", str_replace( '"', '', $csv ) );
array_shift ( $csv );


// 結果ディレクトリを初期化
unlinkRecursive ( $cd . '/result/' );
// noteテキストの結果ディレクトリ初期化
unlinkRecursive ( 'contents/note_text/result/' );


// --------------------------------------------------

$img_dir = 'contents/images/';
$thumb_dir = 'contents/images/';

//$thumb_dir = 'contents/thumb/';

/** 
 * ループここから
 */

// CSV1: データ整形
foreach ( $csv as $v ) {

  // カンマでバラす
  $v = explode ( ",", $v );
  
  $No = $v[0];
  $keymessage = $v[1];
  $imgName = $v[2];
  $noteFlag = $v[3];
  $full_img_name = $v[1] . '-full';
  $thumb_img_name = $v[1] . '-thumb';

  if ( $keymessage !== NULL) {


    /** 
     * テンプレート
     */
    $rel_dir = 'contents/temp';
    $new_dir = 'result/' . $keymessage;
    
    dir_copy($rel_dir, $new_dir);

    /** 
     * HTMLファイル
     */
    $temp = $new_dir . '/index.html';
    $html = $new_dir . '/' . 'index' . '.html';
    rename ( $temp , $html );

    /** 
     * Thumbnail full ( Change a file name )
     */
//    $temp_full = $new_dir . '/poster.png';
////    $new_full = $new_dir . '/' . $full_img_name . '.jpg';
//    $new_full = $new_dir . '/' . 'poster' . '.png';
//    rename ( $temp_full , $new_full );
//
//    /** 
//     * Thumbnail thumb  ( Change a file name )
//     */
//    $thumb_full = $new_dir . '/thumb.png';
////    $new_thumb = $new_dir . '/' . $thumb_img_name . '.jpg';
//    $new_thumb = $new_dir . '/' . 'thumb' . '.png';
//    rename ( $thumb_full , $new_thumb );

    /** 
     * 背景画像
     */
    // 画像名をbgに変更
    $bgimg = $img_dir . $imgName . $bgExtension;
    $newbgimg = $new_dir . '/images/' . $bgName . $bgExtension;
    copy($bgimg, $newbgimg);

    // Thumbnail full  ( Copy a file )
    $new_full_img = $new_dir . '/' . 'poster' . $thumbExtension;
    $full_img_for_full = imagecreatefromjpeg($bgimg);
    $full_size = imagecreatetruecolor(1024, 768);
    
    imagecopyresampled($full_size, $full_img_for_full, 0, 0, 0, 0, 1024, 768, 2048, 1536);
    imagejpeg($full_size,$new_full_img,100);


//    // Thumbnail thumb ( Copy a file )
    $new_thumb_img = $new_dir . '/' . 'thumb' . $thumbExtension;
    $full_img_for_thumb = imagecreatefromjpeg($bgimg);
    $thumb_size = imagecreatetruecolor(200, 150);

    imagecopyresampled($thumb_size, $full_img_for_thumb, 0, 0, 0, 0, 200, 150, 2048, 1536);
    imagejpeg($thumb_size,$new_thumb_img,100);

  }
}
// --------------------------------------------------

// ."\n";

//気分的に吐いてほしいから
echo 'complete＼(^o^)／オワタ';

?>