(async () => {
  const config = window.bmy.shared.config;
  const currentId = await getCurrentKeyMessageId();
  const baseURL = location.href.substr(0, location.href.indexOf(currentId));
  const targetData = await getKeyMessage(config.targetMediaFileName);

  config.sharedDirectory = `${baseURL}${targetData.Id}/${targetData.Media_File_CRC_vod__c}/${config.targetMediaFileName.replace(/\.zip/, '')}/shared/`;

  const script = document.createElement('script');
  script.src = `${config.sharedDirectory}${config.targetScript}`;
  document.body.appendChild(script);

  // 疑似シェアード読み込みに失敗した場合は、ローカルのスクリプトだけロードする
  script.onerror = async () => {
    console.log('load shared failed.');
    await injectFile('', config.localScripts);
  };


  /**********************
    以降はプライベート関数
   **********************/
  async function injectFile(dirpath, list) {
    for (const filepath of list) {
      await loadExtFiles(dirpath, filepath);
    }
  }

  function loadExtFiles(dirpath, filepath) {
    return new Promise(async (resolve, reject) => {
      if(filepath.match(/\.css$/)) {
        const link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.href = `${dirpath}${filepath}`;
        link.onload = resolve;
        document.head.appendChild(link);
      } else if (filepath.match(/\.js$/)){
        const script = document.createElement('script');
        script.src = `${dirpath}${filepath}`;
        script.onload = resolve;
        document.body.appendChild(script);
      } else if (filepath.match(/\.html$/)){
        const res = await axios.get(`${dirpath}${filepath}`);
        const html = res.data;
        document.body.insertAdjacentHTML('beforeend', html);
        resolve();
      }
    });
  }

  function getCurrentKeyMessageId() {
    return new Promise(
      (resolve, reject) => {
        console.log(`---------- START getCurrentKeyMessageId. ----------`);
        console.log(`arguments: ${JSON.stringify(arguments)}`);
        com.veeva.clm.getDataForCurrentObject(
          'KeyMessage',
          'Id',
          result => {
            console.log(`---------- END (normal) getCurrentKeyMessageId. ----------`);
            console.log(JSON.stringify(result));
            if (result.success) {
              resolve(result.KeyMessage.Id);
            } else {
              console.log(`---------- END (abnormal) getCurrentKeyMessageId. ----------`);
              alert(JSON.stringify(result));
              reject(JSON.stringify(result));
            }
          });
      }
    )
  }

  async function getKeyMessage(mediaFileName) {
    return new Promise(
      (resolve, reject) => {
        console.log(`---------- START getKeyMessage. ----------`);
        console.log(`arguments: ${JSON.stringify(arguments)}`);
        com.veeva.clm.queryRecord(
          'Key_Message_vod__c',
          'Id, 	Media_File_CRC_vod__c',
          `WHERE Media_File_Name_vod__c = "${mediaFileName}"`,
          null,
          null,
          result => {
            console.log(JSON.stringify(result));
            if (result.success && result.Key_Message_vod__c.length > 0) {
              console.log(`---------- END (normal) getKeyMessage. ----------`);
              resolve(result.Key_Message_vod__c[0]);
            } else {
              console.log(`---------- END (abnormal) getKeyMessage. ----------`);
              alert(JSON.stringify(result));
              reject(JSON.stringify(result));
            }
          });
      }
    )
  };

}).call(this);
