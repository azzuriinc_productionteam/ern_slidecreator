(function(){
  console.log('index.js start');

  var $pop_up_btn = $('.pop_up_btn');
  var $pop_up_area = $('.pop_up_area');
  var $close_btn_pop = $('.close_btn_pop');

  /** ポップアップ表示 */
  $pop_up_btn.on('touchend',function(event){
    var pop_target = $(this).attr('data-pop');
    event.stopPropagation();
    $('#' + pop_target).addClass('on');


  });

  /** ポップアップ非表示 */
  $close_btn_pop.on('touchend',function(event){
    event.stopPropagation();
    $pop_up_area.removeClass('on');
    //ポップアップ内ノートをリセット
    $('#pop_noteBox').css('display','none');
    $('#pop_noteBtn').addClass("block").removeClass("none");
  });


  /* タブ切り替え */
  var $tab_list = $('.pop_tab li'); //tabのボタン部分
  var $tab_area = $('.tab');　//tabの表示部分

  $tab_list.on('touchend',function() {
    var index = $tab_list.index(this);

    $tab_area.removeClass('on');

    $tab_area.eq(index).addClass('on');
    $tab_list.removeClass('on');
    $(this).addClass('on');
//    myScroll.refresh();
  });


  var myScroll;  /* 縦スクロール */
  myScroll = new IScroll("#pop_scroll", {
    useTransform: true,
    mouseWheel: true,
    tap: true,
    scrollbars: true, /* スクロールバーを表示 */
    interactiveScrollbars: true, /* スクロールバーをドラッグできるようにする */
    scrollbars: 'custom',

  });


	//Veeva Link
	$('[data-goto]').on('touchend',function(e){
		e.preventDefault();
		var slideStr = $(this).attr('data-goto');
		com.veeva.clm.gotoSlide(slideStr);
		}
	);


})(window);
