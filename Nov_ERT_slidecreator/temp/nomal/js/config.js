if (!('bmy' in window)) window.bmy = {};
if (!('shared' in window.bmy)) window.bmy.shared = {};

window.bmy.shared.config = {
  // sharedスクリプトが入っているkeymessageのMediaFileName
  targetMediaFileName: 'ENR_Top_01_01.zip',

  // sharedスクリプト本体の相対パス('shared'以降のパス)
  targetScript: 'js/shared.js',

  // スライド個別で読み込むjsファイル
  localScripts: [
    'js/index.js',
  ],

  // 表示するボタン
  buttons: {
    exam: { //試験概要
      isDisp: false,
      presentationId: '',
      mediaFileName: '',
    },
    sup: { //補足データ
      isDisp: false,
      presentationId: 'ENR_Slide_03A',
      mediaFileName: 'ENR_Slide_03A_01.zip',
    },
    slide: { //スライド一覧
      isDisp: true,
    },
    back: {},
  },
};

// 閉じるボタン表示の設定がなく、試験概要、補足資料、スライド一覧のすべてが非表示の場合、閉じるボタンを表示
window.bmy.shared.config.buttons.back.isDisp = !(
  window.bmy.shared.config.buttons.back.isDisp !== undefined
  || window.bmy.shared.config.buttons.exam.isDisp
  || window.bmy.shared.config.buttons.sup.isDisp
  || window.bmy.shared.config.buttons.slide.isDisp
);
